# Docker Compose testbed

Test/demo of Docker build/save/load for Docker compose

```bash

# Build and save

docker build --target base --tag test-docker-compose-built .
docker save --output test-docker-compose-built.tar test-docker-compose-built

# Remove it for testing
docker rm test-docker-compose-built
docker image rm test-docker-compose-built

# Confirm that we see the build happen here - not loaded
docker compose -f docker-compose.build.yml run --remove-orphans built

# Make sure to clear EVERYTHING out
docker ps --all | grep built
docker rm test-docker-compose-built-run-a0d31bfcce2e  # Use the actual name
docker image rm test-docker-compose-built

# Load it back in
docker load --input test-docker-compose-built.tar

# Then perform the run again and see it working (i.e. not building)
docker compose -f docker-compose.build.yml run --remove-orphans built
```

Some other commmands we saved.

```bash
docker compose -f docker-compose.build.yml run --remove-orphans built
docker compose -f docker-compose.build.yml down --remove-orphans
docker compose -f docker-compose.build.yml up --remove-orphans &
```