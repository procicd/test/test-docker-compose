FROM ubuntu:latest as base

ARG ARG1=default1
ARG ARG2=default2

RUN echo "${ARG1}" > args.txt

RUN echo "base" > target.txt

FROM base

RUN echo "final" > target.txt

